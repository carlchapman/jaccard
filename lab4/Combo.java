package lab4;

import java.util.Arrays;
import java.util.TreeSet;

import org.apache.hadoop.io.Text;

/**
 * The Class Combo parses text into a format more convenient for the Jaccard
 * program.
 */
public class Combo {

	/** The hashes. */
	protected String[] hashes;

	/** The IDs. */
	protected TreeSet<String> IDs;

	/** The document. */
	protected String document;
	
	/** The shingle set. */
	private TreeSet<String> shingleSet;

	/**
	 * Instantiates a new Combo.
	 * 
	 * @param text
	 *            the text used to create a new Combo
	 */
	public Combo(String text) {

		// text should come in looking like:
		// 'hash:hash:...hash:ID-ID-ID-...ID-Document'
		String[] tokens = text.split(Jaccard.DELIM);

		// the last String should be the IDs and Document
		String end = tokens[tokens.length - 1];

		// this should copy all of tokens but the last one
		hashes = Arrays.copyOfRange(tokens, 0, tokens.length - 1);

		// re-use tokens variable, now referencing IDs and the Document
		tokens = end.split(Jaccard.DASH);
		document = tokens[tokens.length - 1];

		// now copy all but the last of tokens and add all to IDs Collection
		IDs = new TreeSet<String>();
		IDs.addAll(Arrays.asList(Arrays.copyOfRange(tokens, 0,
				tokens.length - 1)));
		
		// since it's going to be done anyway, only do it once at initialization
		shingleSet = new TreeSet<String>();
		for (int i = 0; i < document.length() - Jaccard.K_CHARS; i++) {
			shingleSet.add(document.substring(i, i + Jaccard.K_CHARS));
		}
	}

	/**
	 * Finds the Jaccard similarity of another Combo and this Combo.
	 * 
	 * @param c
	 *            the other combo
	 * @return the Jaccard similarity as a fraction of unity
	 */
	public double jaccardSimilarity(Combo c) {
		
		TreeSet<String> union = new TreeSet<String>();
		TreeSet<String> intersection = new TreeSet<String>();
		
		intersection.addAll(shingleSet);
		intersection.retainAll(c.shingleSet);

		union.addAll(shingleSet);
		union.addAll(c.shingleSet);

		double intersectionSize = intersection.size();
		return intersectionSize / union.size();
	}

	/**
	 * Tests if a combo is possibly similar to another - returns true if one of
	 * the minHashes has the same value
	 * 
	 * @param c
	 *            the other Combo
	 * @return true if one of the minHashes has the same value
	 */
	public boolean hasCommonHash(Combo c) {
		for(int i=0; i<hashes.length;i++){
			if(hashes[i].equals(c.hashes[i])){
				return true;
			}
		}
		return false;
	}

	/**
	 * Transforms this Combo to a text format that can be re-parsed into a Combo
	 * later.
	 * 
	 * @return the Combo as Text
	 */
	public Text getComboAsText() {
		return new Text(combineHashes() + combineIDs() + document);
	}

	/**
	 * Adds the IDs.
	 * 
	 * @param givenIDs
	 *            the given IDs
	 */
	public void addIDs(TreeSet<String> givenIDs) {
		IDs.addAll(givenIDs);
	}

	/**
	 * Gets the IDs.
	 * 
	 * @return the IDs
	 */
	public TreeSet<String> getIDs() {
		return IDs;
	}

	/**
	 * Gets the hashes.
	 * 
	 * @return the hashes
	 */
	public String[] getHashes() {
		return hashes;
	}

	/**
	 * Gets the document.
	 * 
	 * @return the document
	 */
	public String getDocument() {
		return document;
	}

	/**
	 * Gets the IDs in the final format required.
	 * 
	 * @return the final IDs
	 */
	public String getfinalIDs() {
		String finalIDs = "";
		for (String ID : IDs) {
			finalIDs += ID + ", ";
		}
		finalIDs = finalIDs.substring(0, finalIDs.length() - 2);
		return finalIDs;
	}

	// *************standard hashcode and equals***************

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IDs == null) ? 0 : IDs.hashCode());
		result = prime * result
				+ ((document == null) ? 0 : document.hashCode());
		result = prime * result + Arrays.hashCode(hashes);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Combo other = (Combo) obj;
		if (IDs == null) {
			if (other.IDs != null)
				return false;
		} else if (!IDs.equals(other.IDs))
			return false;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (!Arrays.equals(hashes, other.hashes))
			return false;
		return true;
	}

	// ********private methods**********

	/**
	 * Combine IDs.
	 * 
	 * @return IDs separated by dashes
	 */
	private String combineIDs() {
		String allIDs = "";
		for (String ID : IDs) {
			allIDs += ID + Jaccard.DASH;
		}
		return allIDs;
	}

	/**
	 * Combine hashes.
	 * 
	 * @return hashes separated by a delimiter
	 */
	private String combineHashes() {
		String allHashes = "";
		for (int i = 0; i < hashes.length; i++) {
			allHashes += hashes[i] + Jaccard.DELIM;
		}
		return allHashes;
	}
}