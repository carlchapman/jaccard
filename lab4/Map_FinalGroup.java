package lab4;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * This mapper groups everything together for a final comparison, by giving them all a key of zero.
 *
 * @author carlchapman
 */
public class Map_FinalGroup extends
		Mapper<LongWritable, Text, IntWritable, Text> {
	
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		context.write(new IntWritable(0), value);
	}
} // end Map_SeparateCombos.class