package lab4;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * The Class Reduce_ClusterDocuments.
 *
 * @author carlchapman
 */
public class Reduce_ClusterDocuments extends
		Reducer<IntWritable, Text, Text, Text> {

	/** The temporary list. */
	LinkedList<Combo> temporaryList;
	
	/** The empty string. */
	private Text emptyString = new Text("");
	
	private static double MIN_SIMILARITY;
	
	@Override
	public void setup(Context jobContext) {
		MIN_SIMILARITY = Double.parseDouble(jobContext.getConfiguration().get("MIN_SIMILARITY"));
		Jaccard.log("MIN_SIMILARITY set to: "+MIN_SIMILARITY+" in Reduce_ClusterDocuments");
	}

	public void reduce(IntWritable key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		temporaryList = new LinkedList<Combo>();

		// put everything that grouped to the minhash into memory
		for (Text t : values) {
			temporaryList.add(new Combo(t.toString()));
		}

		Combo cluster = null;
		while (!temporaryList.isEmpty()) {

			// get the first element, removing it
			cluster = temporaryList.poll();
			Iterator<Combo> it = temporaryList.iterator();
			while (it.hasNext()) {
				Combo c = it.next();
				double similarity = cluster.jaccardSimilarity(c);
				if (similarity < MIN_SIMILARITY) {
					Jaccard.log("similarity of " + c.getfinalIDs()
							+ " to cluster: " + cluster.getfinalIDs() + " :=: "
							+ similarity);
				} else {
					cluster.addIDs(c.getIDs());
					it.remove();
				}
			}
			// write the cluster, even if it didn't match anybody in this
			// group
			context.write(cluster.getComboAsText(), emptyString);
		}
	}// end reduce method

}// end Reduce_ClusterDocuments.class