package lab4.old;

import java.util.Arrays;
import java.util.TreeSet;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class OldCombo {

	private String DELIM;
	private String DASH;

	private String[] hashes;
	private TreeSet<String> IDs;
	private String document;

	public OldCombo(String text, String delim, String dash) {
		DELIM = delim;
		DASH = dash;

		// text should come in looking like:
		// 'hash:hash:...hash:ID-ID-ID-...ID-Document'
		String[] tokens = text.split(DELIM);

		// the last String should be the IDs and Document
		String end = tokens[tokens.length - 1];

		// this should copy all of tokens but the last one
		hashes = Arrays.copyOfRange(tokens, 0, tokens.length - 1);

		// re-use tokens variable, now referencing IDs and the Document
		tokens = end.split(DASH);
		document = tokens[tokens.length - 1];

		// now copy all but the last of tokens and add all to IDs Collection
		IDs = new TreeSet<String>();
		IDs.addAll(Arrays.asList(Arrays.copyOfRange(tokens, 0,
				tokens.length - 1)));
	}

	private String combineIDs() {
		String allIDs = "";
		for (String ID : IDs) {
			allIDs += ID + DASH;
		}
		return allIDs;
	}

	private String combineHashes() {
		String allHashes = "";
		for (int i = 1; i < hashes.length; i++) {
			allHashes += hashes[i] + DELIM;
		}
		return allHashes;
	}

	public Text getValue() {
		return new Text(combineHashes() + combineIDs() + document);
	}

	public IntWritable getKey() {
		return new IntWritable(Integer.parseInt(hashes[0]));
	}

	// for making a combo from the gold standard
	public OldCombo(int[] givenHashes, OldCombo combo, String delim, String dash) {
		DELIM = delim;
		DASH = dash;
		hashes = new String[givenHashes.length];
		for (int i = 0; i < givenHashes.length; i++) {
			hashes[i] = String.valueOf(givenHashes[i]);
		}
		IDs = new TreeSet<String>();
		IDs.addAll(combo.IDs);
		document = combo.document;
	}

	public void addIDs(TreeSet<String> givenIDs) {
		IDs.addAll(givenIDs);
	}

	public TreeSet<String> getIDs() {
		return IDs;
	}

	public String[] getHashes() {
		return hashes;
	}

//	public int[] getIntegerHashes() {
//		int[] intHashes = new int[hashes.length];
//		for(int i=0;i<hashes.length;i++){
//			intHashes[i] = Integer.parseInt(hashes[i]);
//		}
//		return intHashes;
//	}
	
	public String getDocument() {
		return document;
	}

	public String getfinalIDs() {
		String finalIDs = "";
		for (String ID : IDs) {
			finalIDs += ID + ", ";
		}
		finalIDs = finalIDs.substring(0, finalIDs.length() - 2);
		return finalIDs;
	}

	/**
	 * this method assumes that the givenHashes array is at the same size as the
	 * hashes array
	 * 
	 * @param n
	 * @param givenHashes
	 * @return
	 */
	public boolean atMostNMismatches(int maxMisMatches, int[] givenHashes) {
		int nHashesThatMatch = 0;
		for (int i = 0; i < givenHashes.length; i++) {
			if (givenHashes[i] == Integer.parseInt(hashes[i])) {
				nHashesThatMatch++;
			}
		}
		return givenHashes.length - nHashesThatMatch <= maxMisMatches;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IDs == null) ? 0 : IDs.hashCode());
		result = prime * result
				+ ((document == null) ? 0 : document.hashCode());
		result = prime * result + Arrays.hashCode(hashes);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OldCombo other = (OldCombo) obj;
		if (IDs == null) {
			if (other.IDs != null)
				return false;
		} else if (!IDs.equals(other.IDs))
			return false;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (!Arrays.equals(hashes, other.hashes))
			return false;
		return true;
	}

	public double jaccardSimilarity(OldCombo c, int kSizedShingles) {
		int identicalShingles = 0;
		double totalShingles = 0;
		String otherDocument = c.getDocument();
		for(int i=0;i<document.length()-kSizedShingles;i++){
			for(int j=0;j<kSizedShingles;j++){
				String thisShingle = document.substring(i, i + kSizedShingles);
				String thatShingle = otherDocument.substring(i, i + kSizedShingles);
				if(thisShingle.equals(thatShingle)){
					identicalShingles++;
				}
			}
			totalShingles++;
		}
		return identicalShingles/totalShingles;
	}
}