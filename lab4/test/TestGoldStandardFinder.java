package lab4.test;

import lab4.old.GoldStandardFinder;

public class TestGoldStandardFinder {
	

	public static void main(String[] args) throws Exception {
		GoldStandardFinder finder = new GoldStandardFinder(2,0.2,5);
		
		int[] testHashes1 = {11,16,14,115,9, 12, 13, 12, 12,12,12,1,12, 12,12,12,12, 12,12,12,};
		int[] testHashes2 = {21,26,24,125,8, 22, 22, 22, 22,22,22,21, 22, 22, 22, 22,28, 22, 22, 22, 22,22};
		int counter = 0;
		for(int i=0;i<testHashes1.length;i++){
			if(finder.findStandard(0, testHashes1[i],1)){
				break;
			}
			if(finder.findStandard(1, testHashes2[i],1)){
				break;
			}
			counter++;
		}
		int[] gold = finder.getGoldStandard();
		System.out.println("counter: "+counter+" gold[0]: "+gold[0]+" gold[1]: "+gold[1]);
	}
}
