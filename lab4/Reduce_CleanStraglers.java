package lab4;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * The Class Reduce_CleanStraglers - this is very similar to
 * Reduce_ClusterDocuments except it expects that almost all documents are
 * dissimilar, and so it tries to group the last straglers - documents that by
 * chance didn't group by minhash - with their appropriate clusters in a
 * different way.
 * 
 * @author carlchapman
 */
public class Reduce_CleanStraglers extends
		Reducer<IntWritable, Text, Text, Text> {

	/** The temporary list. */
	LinkedList<Combo> temporaryList;
	private static double MIN_SIMILARITY;
	private static int clusterCounter = 0;
	
	@Override
	public void setup(Context jobContext) {
		MIN_SIMILARITY = Double.parseDouble(jobContext.getConfiguration().get("MIN_SIMILARITY"));
		Jaccard.log("MIN_SIMILARITY set to: "+MIN_SIMILARITY+" in Reduce_CleanStraglers");
	}

	public void reduce(IntWritable key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		temporaryList = new LinkedList<Combo>();

		// put everything into memory
		for (Text t : values) {
			temporaryList.add(new Combo(t.toString()));
		}

		Combo cluster = null;
		while (!temporaryList.isEmpty()) {

			// get the first element, removing it
			cluster = temporaryList.poll();

			Iterator<Combo> it = temporaryList.iterator();
			while (it.hasNext()) {
				Combo c = it.next();
				if (cluster.hasCommonHash(c)) {
					double similarity = cluster.jaccardSimilarity(c);
					if (similarity >= MIN_SIMILARITY) {
						Jaccard.log("similarity of " + c.getfinalIDs()
								+ " to cluster: " + cluster.getfinalIDs()
								+ " :=: " + similarity);
						cluster.addIDs(c.getIDs());
						it.remove();
					}
				}
			}
			// this can take a while, so avoid timeouts
			context.progress();
			
			int size = cluster.getIDs().size();
			if (size != 100) {
				Jaccard.log("found cluster with size: " + size + " details: "
						+ cluster.getfinalIDs() + "\t" + cluster.getDocument());
			}
			// write the cluster in the final format
			context.write(new Text(cluster.getfinalIDs()),
					new Text(cluster.getDocument()));
			clusterCounter++;
			if (clusterCounter % 100 == 0) {
				Jaccard.log("clusterCounter: " + clusterCounter);
			} else if (clusterCounter > 1000) {
				Jaccard.log("ERROR!!! clusterCounter: " + clusterCounter);
			}
		}
	}// end reduce method

}// end Reduce_CleanStraglers.class