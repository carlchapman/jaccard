package lab4;

import java.io.IOException;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * This mapper breaks off the first minHash and uses it as a key.
 *
 * @author carlchapman
 */
public class Map_SeparateCombos extends
		Mapper<LongWritable, Text, IntWritable, Text> {
	
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {

		String line = value.toString();
		int delimIndex = line.indexOf(Jaccard.DELIM);
		context.write(
				new IntWritable(Integer.parseInt(line.substring(0,
						delimIndex))),
				new Text(line.substring(delimIndex + 1)));
	}
} // end Map_SeparateCombos.class