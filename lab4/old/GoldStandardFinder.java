package lab4.old;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * The GoldStandardFinder finds the most commonly used minHash for each of a
 * given number of minHash groups (each group used the same seed in its hash
 * function). The minimum number of sample minHashes for each group to look at
 * and the percent of hashes that must be the same before declaring one minHash
 * the gold standard (most common hash for a group) are adjustable arguments
 * passed in at construction. After construction, minHashes must be passed in
 * one by one until the gold standard is found for all groups. After that, the
 * gold standard should be retrieved and used to sort out what documents belong
 * in the cluster.
 * 
 * @author carlchapman
 * 
 */
public class GoldStandardFinder {

	private double goldBar;
	private int minSamples;
	public GoldDigger[] columns;

	public GoldStandardFinder(int nHashes, double goldBar, int minimumNumberOfSamples) {
		this.goldBar = goldBar;
		this.minSamples = minimumNumberOfSamples;
		columns = new GoldDigger[nHashes];
		for (int i = 0; i < nHashes; i++) {
			columns[i] = new GoldDigger();
		}
	}

	public boolean findStandard(int hashIndex, int hashToPut, int nIDs) {
		if (!columns[hashIndex].hasFoundGold()) {
			for(int i=0;i<nIDs;i++){
				columns[hashIndex].putHash(hashToPut);	
			}
		}
		return hasFoundStandard();
	}

	private boolean hasFoundStandard() {
		boolean hasFound = true;
		for (GoldDigger digger : columns) {
			hasFound = hasFound && digger.hasFoundGold();
		}
		return hasFound;
	}

	/**
	 * Note - this should only be called after hasFoundStandard returns true.
	 * 
	 * @return an array of ints very likely to be a gold standard that Documents
	 *         from a single cluster should usually have as minhashes
	 */
	public int[] getGoldStandard() {
		int[] goldStandard = new int[columns.length];
		for (int i = 0; i < columns.length; i++) {
			goldStandard[i] = columns[i].getGold();
		}
		return goldStandard;
	}

	/**
	 * each GoldDigger is responsible for a particular hash type (based on a
	 * unique seed) and tries to find a specific minimum hash value that occurs
	 * commonly among a group of documents/clusters. It does this by requiring
	 * that a hash be above a certain proportion of all the hashes given to it
	 * (after a minimum number of samples have been given to it, as the first is
	 * likely to cause it to be 100%).
	 * 
	 * @author carlchapman
	 * 
	 */
	public class GoldDigger {

		private boolean foundGold;
		private double counter;
		private HashMap<Integer, Integer> hashCounter;

		public GoldDigger() {
			foundGold = false;
			hashCounter = new HashMap<Integer, Integer>();
			counter = 0.0;
		}

		public boolean hasFoundGold() {
			return foundGold;
		}

		public void putHash(int hashToPut) {
			counter++;
			Integer key = new Integer(hashToPut);
			Integer count = hashCounter.get(key);
			if (count == null) {
				count = new Integer(0);
			}
			count = count + 1;
			if (counter > minSamples && (count / counter) > goldBar) {
				foundGold = true;
			}
			hashCounter.put(key, count);
		}

		/**
		 * This assumes that it is being called only after gold is found; at
		 * least one hash must be put before this is called or this method will
		 * throw a null pointer exception.
		 * 
		 * @return The hash for this minHash group that has been put into the
		 *         hashCounter the most times
		 */
		public int getGold() {
			Entry<Integer, Integer> goldenEntry = null;
			Integer largestCount = Integer.MIN_VALUE;
			for (Entry<Integer, Integer> entry : hashCounter.entrySet()) {
				if (entry.getValue() > largestCount) {
					largestCount = entry.getValue();
					goldenEntry = entry;
				}
			}
			return goldenEntry.getKey();
		}
	}

}
