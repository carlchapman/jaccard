package lab4;

import java.io.IOException;
import java.util.Random;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * The Class Map_FindMinHashes creates a number of minhashes for each document
 * that can be used to group similar documents together.
 * 
 * @author carlchapman
 */
public class Map_FindMinHashes extends
		Mapper<LongWritable, Text, IntWritable, Text> {

	private static int N_HASHES;

	@Override
	public void setup(Context jobContext) {
		N_HASHES = Integer.parseInt(jobContext.getConfiguration().get("N_HASHES"));
		Jaccard.log("N_HASHES set to: "+N_HASHES);
	}

	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {

		long baseSeed = 3719474739L;
		Random gen = new Random(baseSeed);
		int[] seeds = new int[N_HASHES];
		for (int i = 0; i < N_HASHES; i++) {
			seeds[i] = gen.nextInt();
		}

		String line = value.toString();

		// make a place to store hashes
		int[] minHashes = new int[N_HASHES];
		for (int i = 0; i < N_HASHES; i++) {
			minHashes[i] = Integer.MAX_VALUE;
		}

		// find the minhashes for the document
		int cursorStart = line.indexOf('-') + 1;
		for (int cursor = cursorStart; cursor < line.length() - Jaccard.K_CHARS; cursor++) {
			for (int j = 0; j < N_HASHES; j++) {
				minHashes[j] = Math.min(
						minHashes[j],
						Jaccard.hash(line.substring(cursor, cursor
								+ Jaccard.K_CHARS), seeds[j]));
			}
		}

		// transform (all but first) hashes to a String format
		String hashes = "";
		for (int i = 1; i < N_HASHES; i++) {
			hashes += minHashes[i] + Jaccard.DELIM;
		}

		// emit the first hash as key, followed by rest of hashes and
		// document
		context.write(new IntWritable(minHashes[0]), new Text(hashes + line));
	}
} // end Map_FindMinHashes.class