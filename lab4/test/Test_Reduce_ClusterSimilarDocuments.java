package lab4.test;

import java.io.IOException;
import java.util.LinkedList;

import lab4.old.GoldStandardFinder;
import lab4.old.OldCombo;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class Test_Reduce_ClusterSimilarDocuments {
	static LinkedList<OldCombo> temporaryList;
	private static final int MAX_MISMATCHES = 1;
	private static final int MIN_SAMPLES = 46;
	private static final double MIN_CONSISTENCY = 0.66;
	private static final String DELIM = ":";
	private static final String DASH = "-";

	private static String c1 = "6:7:8:23-45-54-tired"; // 3 IDs
	private static String c2 = "6:7:8:12-1-2-cats";// 3 IDs
	private static String c3 = "6:7:8:13-8-9-delight";// 3 IDs
	private static String c4 = "6:7:8:14-78-in";// 2 IDs
	private static String c5 = "6:7:9:15-77-catnip";// 2 IDs
	private static String c6 = "4:7:8:16-79-pillows";// 2 IDs
	private static String c7 = "6:14:12:17-80-sitting";// 2 IDs
	private static String c8 = "12:14:8:18-81-by";// 2 IDs
	private static String c9 = "11:14:8:19-82-the";// 2 IDs
	private static String c10 = "18:28:38:20-83-99-1000-7654-43422-5435425432542-window";// 7 IDs
	
	private static LinkedList<Text> list;

	private static String[] comboStrings = { c1, c2, c3, c4, c5, c6, c7, c8, c9, c10 };

	public static void main(String[] args) throws Exception {
		 list = new LinkedList<Text>();
		 for(String s : comboStrings){
			 list.add(new Text(s));
		 }
		 
		 IntWritable dumbKey = new IntWritable(10);
		 reduce(dumbKey, list);

	}

	/**
	 * 
	 */
	public static void reduce(IntWritable key, Iterable<Text> values)
			throws IOException, InterruptedException {
		temporaryList = new LinkedList<OldCombo>();

		int iterationCounter = 0;
		GoldStandardFinder finder = null;
		boolean goldStandardFound = false;
		int[] gold = null;
		OldCombo cluster = null;

		for (Text t : values) {
			iterationCounter++;
			OldCombo parsedLine = new OldCombo(t.toString(), DELIM, DASH);

			// initialize finder using size of array in parsedLine
			if (finder == null) {
				finder = new GoldStandardFinder(parsedLine.getHashes().length,
						MIN_CONSISTENCY, MIN_SAMPLES);
			}

			if (!goldStandardFound) {
				// add each hash to the finder one by one
				for (int i = 0; i < parsedLine.getHashes().length; i++) {

					// if the gold standard has been found...
					if (finder.findStandard(i,
							Integer.parseInt(parsedLine.getHashes()[i]),
							parsedLine.getIDs().size())) {

						// get the gold standard
						gold = finder.getGoldStandard();
						goldStandardFound = true;

						// get the cluster
						cluster = getCluster(cluster, gold, parsedLine, "goldStandard found while iterating through");

						// stop adding hashes to the finder
						break;
					}
				}

				// before the gold standard has been found, keep all the
				// combos in a list
				temporaryList.add(parsedLine);

				// after the gold standard is found, separate values
				// according to it
			} else if (parsedLine.atMostNMismatches(MAX_MISMATCHES, gold)) {
				cluster.addIDs(parsedLine.getIDs());
			} else {

				// write the mismatches so they will (hopefully) get grouped
				// with their cluster in another round
				System.out.println("\n found gold but this mismatches");
				System.out.println(parsedLine.getKey().toString() + "\t" + parsedLine.getIDs().size() +"::" + parsedLine.getValue().toString());
			}
		}// end of iteration through the values

		// if gold standard was not found because there were too few values,
		// use what gold you can get
		if (!goldStandardFound && iterationCounter < MIN_SAMPLES) {
			gold = finder.getGoldStandard();

			// if you can find a combo that is close to the gold, use it for
			// its document to make the cluster
			OldCombo exampleDoc = findExampleDoc(gold, temporaryList);
			if (exampleDoc != null) {
				cluster = getCluster(cluster, gold, exampleDoc, "gold not found and iterationCounter < MIN_SAMPLES");
				goldStandardFound = true;
			}


		}
		
		// if there were enough samples but they were not consistent
		// enough, or somehow the gold didn't match anything well enough
		// to choose an example (doubtful) then emit everything
		// separately
		if (!goldStandardFound || cluster == null) {
			for (OldCombo c : temporaryList) {
				System.out.println("\n last ditch effort");
				System.out.println(c.getKey().toString() + "\t"+ c.getIDs().size() +"::"+ c.getValue().toString());
			}

			// a gold standard was found and the cluster is not null
		} else {
			System.out.println("\n normal emission of cluster according to gold standard");
			System.out.println(cluster.getKey().toString() + "\t"+ cluster.getIDs().size() +"::" +cluster.getValue().toString());
		}

	}

	private static OldCombo findExampleDoc(int[] gold, LinkedList<OldCombo> temp) {
		for (OldCombo c : temp) {
			if (c.atMostNMismatches(MAX_MISMATCHES, gold)) {
				temp.remove(c);
				return c;
			}
		}
		return null;
	}

	private static OldCombo getCluster(OldCombo givenCluster, int[] gold, OldCombo exampleDoc,
			String context) throws IOException, InterruptedException {
		// prepare a combo to emit for all combos that match
		// the standard well enough
		givenCluster = new OldCombo(gold, exampleDoc, DELIM, DASH);

		// separate the temporary list according to the gold
		// standard
		for (OldCombo combo : temporaryList) {
			if (combo.atMostNMismatches(MAX_MISMATCHES, gold)) {
				givenCluster.addIDs(combo.getIDs());
			} else {

				// write the mismatches so they will
				// (hopefully) get grouped with their
				// cluster in another round
				System.out.println("\nIn getClusterMethod: "+context);
				System.out.println(combo.getKey().toString() + "\t"+ combo.getIDs().size() +"::" + combo.getValue().toString());
			}
		}
		return givenCluster;
	}

}// end Reduce_ClusterDocuments.class
