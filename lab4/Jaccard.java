package lab4;

import java.nio.ByteBuffer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

/**
 * The Class Jaccard clusters documents with the given minimum Jaccard
 * similarity together.
 */
public class Jaccard extends Configured implements Tool {

	/** possible valid inputs. */
	private static String[] validInputs = { "test-files", "group-files",
			"second-group-files" };

	/** The size of the corpus. */
	private static final int CORPUS_SIZE = 100000;

	/** A constant for allowed error. */
	private static final double EPSILON = 0.001;

	/** The number of clustering rounds before the final grouping */
	private static final int R_ROUNDS = 2;

	/** The size of shingles to use. */
	public static final int K_CHARS = 9;

	/** The delimiter for separating hashes. */
	public static final String DELIM = ":";

	/** The dash used to separate IDs from documents. */
	public static final String DASH = "-";
	
	/**
	 * The Constant N_HASHES is calculated dynamically based on the corpus size
	 * and the desired similarity of documents. For less similar clusters,
	 * collisions between dissimilar documents are more likely and so more
	 * minHashes are needed to ensure that similar documents are likely to have
	 * at least one minHash in common.
	 */
	static int N_HASHES;

	/**
	 * The minimum Jaccard similarity that documents must have in order to
	 * cluster together.
	 */
	static double MIN_SIMILARITY;

	/** a static logger to write messages to the logs. */
	private static Logger log = Logger.getLogger(Jaccard.class.getName());

	/**
	 * The main method - starts a ToolRunner.
	 * 
	 * @param args
	 *            - corpus name
	 * @throws Exception
	 *             - passes any exception that may occur down the stack
	 */
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new Jaccard(), args);
		System.exit(res);
	}

	/**
	 * A driver for the Jaccard program.
	 */
	public int run(String[] args) throws Exception {
		Configuration conf = new Configuration();
		new GenericOptionsParser(conf, args).getRemainingArgs();

		// get given dataset to use, if any (test-group by default)
		String dataSetName = getDataSet(args);

		// set the Jaccard Similarity to a given argument, if any valid argument
		// was given, or 0.9 by default
		MIN_SIMILARITY = getJaccardSimilarity(args);				
		N_HASHES = computeNHashes(R_ROUNDS, MIN_SIMILARITY, EPSILON,
				CORPUS_SIZE);
		
		//pass the dynamic info into the configuration
		conf.set("MIN_SIMILARITY", String.valueOf(MIN_SIMILARITY));
		conf.set("N_HASHES", String.valueOf(N_HASHES));

		log("nHashes: " + N_HASHES + " for minSimilarity: " + MIN_SIMILARITY
				+ " and epsilon: " + EPSILON + " and Corpus size: "
				+ CORPUS_SIZE);

		// set up paths for input, temporary output and final output
		String input = "/datasets/Lab4/" + dataSetName;
		String temp1 = "/user/carl1978/Lab4/temp1";
		String temp2 = "/user/carl1978/Lab4/temp2";
		String output = "/user/carl1978/Lab4/" + dataSetName + "Answer";

		// automatically delete the directories that we're going to use
		FileSystem fs = FileSystem.get(new Configuration());
		fs.delete(new Path(temp1), true);
		fs.delete(new Path(temp2), true);
		fs.delete(new Path(output), true);
		log("deleted paths");

		// **** find the minHashes for each document ****
		// **********************************************

		Job hashJob = new Job(conf, "find minhashes, start clustering");
		hashJob.setJarByClass(Jaccard.class);
		hashJob.setNumReduceTasks(2);

		hashJob.setOutputKeyClass(IntWritable.class);
		hashJob.setOutputValueClass(Text.class);

		hashJob.setMapperClass(Map_FindMinHashes.class);
		hashJob.setReducerClass(Reduce_ClusterDocuments.class);

		hashJob.setInputFormatClass(TextInputFormat.class);
		hashJob.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(hashJob, new Path(input));
		FileOutputFormat.setOutputPath(hashJob, new Path(temp1));

		hashJob.waitForCompletion(true);

		log("finished round 1 of clustering");

		// **** cluster documents with the required Jaccard Similarity ****
		// ****************************************************************

		Job clusterJob1 = new Job(conf, "cluster job 1");

		clusterJob1.setJarByClass(Jaccard.class);
		clusterJob1.setNumReduceTasks(2);

		clusterJob1.setOutputKeyClass(IntWritable.class);
		clusterJob1.setOutputValueClass(Text.class);

		clusterJob1.setMapperClass(Map_SeparateCombos.class);
		clusterJob1.setReducerClass(Reduce_ClusterDocuments.class);

		clusterJob1.setInputFormatClass(TextInputFormat.class);
		clusterJob1.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(clusterJob1, new Path(temp1));
		FileOutputFormat.setOutputPath(clusterJob1, new Path(temp2));

		clusterJob1.waitForCompletion(true);
		log("finished round 2 of clustering");

		// **** cluster documents that, by chance, didn't cluster by minHash ***
		// *********************************************************************

		Job writeOutputJob = new Job(conf, "write output");

		writeOutputJob.setJarByClass(Jaccard.class);
		writeOutputJob.setNumReduceTasks(1);

		writeOutputJob.setOutputKeyClass(IntWritable.class);
		writeOutputJob.setOutputValueClass(Text.class);

		// if the job is not finished, the reducer should create the file
		writeOutputJob.setMapperClass(Map_FinalGroup.class);
		writeOutputJob.setReducerClass(Reduce_CleanStraglers.class);

		writeOutputJob.setInputFormatClass(TextInputFormat.class);
		writeOutputJob.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(writeOutputJob, new Path(temp2));
		FileOutputFormat.setOutputPath(writeOutputJob, new Path(output));

		writeOutputJob.waitForCompletion(true);
		log("finished writing clusters");

		return 0;
	}



	/**
	 * calculate the number of hashes to use to guarantee an EPSILON probability
	 * of one stragler, based on constants.
	 * 
	 * The idea here is to guarantee that the probability that...two documents
	 * that have an actual Jaccard similarity greater than the required
	 * minimum and therefore belong in the same cluster, will have at least one
	 * minHash in common...is greater than Epsilon. I'm unable to create a proof
	 * for this, so it can be considered potentially ineffective. The only
	 * certain thing is that for smaller given minimum Jaccard similarity values
	 * and/or larger corpuses and/or smaller epsilons, a larger number of hash
	 * numbers is produced.
	 * 
	 * @return
	 */
	private int computeNHashes(int nRoundsOfClustering, double minSimilarity,
			double Epsilon, int corpusSize) {

		double collisionProbability = 1 - minSimilarity;
		double desiredError = Epsilon / corpusSize;

		// because the log_BASE[X](N) = log(N)/log(X)
		double logErrorBaseCollisionProbability = Math.log10(desiredError)
				/ Math.log10(collisionProbability);
		return (int) ((Math.ceil(logErrorBaseCollisionProbability) + nRoundsOfClustering));
	}

	/**
	 * Takes the standard args array and returns a valid input. If an invalid
	 * input is given, it prints that to the log and exits
	 * 
	 * @param args
	 *            - standard String array of args as passed in through command
	 *            line invocation
	 * @return - a validated input
	 */
	private static String getDataSet(String[] args) {

		// if there is any input...
		if (args.length > 0) {
			try {
				String input = (args[0]);

				// make sure it is a valid input
				for (String s : validInputs) {
					if (input.equals(s)) {
						return input;
					}
				}
				log("invalid input: " + input);
				System.exit(-1);

				// log exceptions...
			} catch (Exception e) {
				for (StackTraceElement trace : e.getStackTrace()) {
					log(trace.toString(), false);
				}
				System.exit(-1);
			}
		}
		return validInputs[0];
	}

	/**
	 * Gets the jaccard similarity from args.
	 * 
	 * @param args
	 *            the args
	 * @return the jaccard similarity
	 */
	private double getJaccardSimilarity(String[] args) {
		if (args.length > 1) {
			double givenDouble = Double.parseDouble(args[1]);
			if (givenDouble >= 0.0 && givenDouble <= 1.0) {
				return givenDouble;
			} else {
				return 0.9;
			}
		} else {
			return 0.9;
		}

	}

	/**
	 * logs messages with some decoration so that they stand out in the console.
	 * 
	 * @param message
	 *            The message to log.
	 */
	protected static void log(String message) {
		log(message, true);
	}

	/**
	 * logs messages.
	 * 
	 * @param message
	 *            The message to log.
	 * @param decorate
	 *            True will decorate the log, false will not.
	 */
	protected static void log(String message, boolean decorate) {
		if (decorate)
			log.info("**************************");

		log.info(message);

		if (decorate)
			log.info("**************************");
	}

	/**
	 * given hash function.
	 * 
	 * @param content
	 *            - the String to be hashed
	 * @param seed
	 *            - a seed to make a unique or repeatable hash function
	 * @return - an int hash of the String
	 */
	public static int hash(String content, int seed) {

		int m = 0x5bd1e995;
		int r = 24;

		int len = content.length();
		byte[] work_array = null;

		int h = seed ^ content.length();

		int offset = 0;

		while (len >= 4) {
			work_array = new byte[4];
			ByteBuffer buf = ByteBuffer.wrap(content.substring(offset,
					offset + 4).getBytes());

			int k = buf.getInt();
			k = k * m;
			k ^= k >> r;
			k *= m;
			h *= m;
			h ^= k;

			offset += 4;
			len -= 4;
		}

		switch (len) {
		case 3:
			h ^= work_array[2] << 16;
		case 2:
			h ^= work_array[1] << 8;
		case 1:
			h ^= work_array[0];
			h *= m;
		}

		h ^= h >> 13;
		h *= m;
		h ^= h >> 15;

		return h;
	}
}