package lab4.old;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

public class Clusters extends Configured implements Tool {

	/**
	 * a static logger to write messages to the console
	 */
	private static Logger log = Logger.getLogger(Clusters.class.getName());

	/**
	 * possible valid inputs
	 */
	private static String[] validInputs = { "test-files", "group-files", "second-group-files" };

	private static final int N_HASHES = 20;
	private static final int K_CHARS = 9;
	private static final String DELIM = ":";
	private static final String DASH = "-";
	private static final String notFinishedFlag = "/user/carl1978/notFinishedFlag";

	/**
	 * The main method - starts a ToolRunner.
	 * 
	 * @param args
	 *            - corpus name
	 * @throws Exception
	 *             - passes any exception that may occur down the stack
	 */
	public static void main(String[] args) throws Exception {

		// Process all MapReduce options if any and then run the MapReduce
		// program
		// Configuration conf = new Configuration();
		//
		// String[] otherArgs = new GenericOptionsParser(conf,
		// args).getRemainingArgs();
		int res = ToolRunner.run(new Configuration(), new Clusters(), args);
		System.exit(res);
	}

	public int run(String[] args) throws Exception {
		Configuration conf = new Configuration();
		new GenericOptionsParser(conf, args).getRemainingArgs();

		// get input, if any
		String dataSetName = getInput(args);
		log("doing " + dataSetName);
		
		//adjust the clustering control static variables according to the dataset
		adjustControls(dataSetName);

		// set up paths for input, temporary output and final output
		String input = "/datasets/Lab4/" + dataSetName;
		String temp1 = "/user/carl1978/Lab4/temp1";
		String temp2 = "/user/carl1978/Lab4/temp2";
		String output = "/user/carl1978/Lab4/" + dataSetName + "Answer";

		// automatically delete the directories that we're going to use
		FileSystem fs = FileSystem.get(new Configuration());
		fs.delete(new Path(temp1), true);
		fs.delete(new Path(temp2), true);
		fs.delete(new Path(notFinishedFlag), true);
		fs.delete(new Path(output), true);
		log("deleted paths");

		// **** find the minHashes for each document ****
		// **********************************************

		Job hashJob = new Job(conf, "find minhashes, start clustering");
		hashJob.setJarByClass(Clusters.class);
		hashJob.setNumReduceTasks(2);

		hashJob.setOutputKeyClass(IntWritable.class);
		hashJob.setOutputValueClass(Text.class);

		hashJob.setMapperClass(Map_FindMinHashes.class);
		hashJob.setReducerClass(Reduce_ClusterSimilarDocuments.class);

		hashJob.setInputFormatClass(TextInputFormat.class);
		hashJob.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(hashJob, new Path(input));
		FileOutputFormat.setOutputPath(hashJob, new Path(temp1));

		hashJob.waitForCompletion(true);

		log("finished round 1 of clustering");

		// **** do clustering in a loop till each cluster has 100 IDs ****
		// ***************************************************************

		int clusterJobCounter = 1;
		long startTime = System.currentTimeMillis();;
		int minutesPassed = 0;
		do {
			Job clusterJob = new Job(conf, "cluster job " + clusterJobCounter);

			// assume that the job is finished.
			fs.delete(new Path(notFinishedFlag), true);

			clusterJob.setJarByClass(Clusters.class);
			clusterJob.setNumReduceTasks(1);

			clusterJob.setOutputKeyClass(IntWritable.class);
			clusterJob.setOutputValueClass(Text.class);

			// if the job is not finished, the reducer should create the file
			clusterJob.setMapperClass(Map_SeparateOldCombos.class);
			clusterJob.setReducerClass(Reduce_ClusterSimilarDocuments.class);

			clusterJob.setInputFormatClass(TextInputFormat.class);
			clusterJob.setOutputFormatClass(TextOutputFormat.class);

			// alternate between two temporary files
			if (clusterJobCounter % 2 == 0) {
				fs.delete(new Path(temp1), true);
				FileInputFormat.addInputPath(clusterJob, new Path(temp2));
				FileOutputFormat.setOutputPath(clusterJob, new Path(temp1));
			} else {
				fs.delete(new Path(temp2), true);
				FileInputFormat.addInputPath(clusterJob, new Path(temp1));
				FileOutputFormat.setOutputPath(clusterJob, new Path(temp2));
			}
			clusterJob.waitForCompletion(true);

			log("finished round " + clusterJobCounter + " of clustering");
			clusterJobCounter++;
			minutesPassed = (int) ((startTime - System.currentTimeMillis())/60000);
		} while (fs.exists(new Path(notFinishedFlag)) && minutesPassed < 25);
		
		// **** write answers ****
		// ***********************

		Job writeJob = new Job(conf, "write clusters of similar documents");
		writeJob.setJarByClass(Clusters.class);
		writeJob.setNumReduceTasks(1);

		writeJob.setOutputKeyClass(IntWritable.class);
		writeJob.setOutputValueClass(Text.class);

		writeJob.setMapperClass(Map_SeparateOldCombos.class);
		writeJob.setReducerClass(Reduce_WriteAnswers.class);

		writeJob.setInputFormatClass(TextInputFormat.class);
		writeJob.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(writeJob, new Path(temp2));
		FileOutputFormat.setOutputPath(writeJob, new Path(output));

		writeJob.waitForCompletion(true);

		log("finished writing clusters");

		return 0;
	}

	private void adjustControls(String dataSetName) {
		if(dataSetName.equals(validInputs[2])){
			Reduce_ClusterSimilarDocuments.setMaxMisMatches(6);
			Reduce_ClusterSimilarDocuments.setMinSamples(6);
			Reduce_ClusterSimilarDocuments.setMinConsistency(0.6);
		}
	}

	// ************************************
	// **** Mapper and Reducer Classes ****
	// ************************************
	// **** In order of appearance ********

	/*
	 * Job 1
	 */

	/**
	 * 
	 * @author carlchapman
	 * 
	 */
	public static class Map_FindMinHashes extends
			Mapper<LongWritable, Text, IntWritable, Text> {

		/**
		 * 
		 */
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			long baseSeed = 3719474739L;
			Random gen = new Random(baseSeed);
			int[] seeds = new int[N_HASHES];
			for (int i = 0; i < N_HASHES; i++) {
				seeds[i] = gen.nextInt();
			}

			String line = value.toString();

			// make a place to store hashes
			int[] minHashes = new int[N_HASHES];
			for (int i = 0; i < N_HASHES; i++) {
				minHashes[i] = Integer.MAX_VALUE;
			}

			// find the minhashes for the document
			int cursorStart = line.indexOf('-') + 1;
			for (int cursor = cursorStart; cursor < line.length() - K_CHARS; cursor++) {
				for (int j = 0; j < N_HASHES; j++) {
					minHashes[j] = Math.min(
							minHashes[j],
							hash(line.substring(cursor, cursor + K_CHARS),
									seeds[j]));
				}
			}

			// transform (all but first) hashes to a String format
			String hashes = "";
			for (int i = 1; i < N_HASHES; i++) {
				hashes += minHashes[i] + DELIM;
			}

			// emit the first hash as key, followed by rest of hashes and
			// document
			context.write(new IntWritable(minHashes[0]),
					new Text(hashes + line));
		}
	} // end Map_FindMinHashes.class

	/**
	 * 
	 * @author carlchapman
	 * 
	 */
	public static class Reduce_ClusterSimilarDocuments extends
			Reducer<IntWritable, Text, IntWritable, Text> {

		LinkedList<OldCombo> temporaryList;
		private static int MAX_MISMATCHES = 4;
		private static int MIN_SAMPLES = 5;
		private static double MIN_CONSISTENCY = 0.80;
		private static boolean haveWrittenToFlagFile = false;

		/**
		 * 
		 */
		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			temporaryList = new LinkedList<OldCombo>();
			FileSystem fs = FileSystem.get(context.getConfiguration());
			
			int uniqueIDCounter = 0;
			GoldStandardFinder finder = null;
			boolean goldStandardFound = false;
			int[] gold = null;
			OldCombo cluster = null;

			for (Text t : values) {
				OldCombo parsedLine = new OldCombo(t.toString(), DELIM, DASH);
				uniqueIDCounter += parsedLine.getIDs().size();

				// initialize finder using size of array in parsedLine
				if (finder == null) {
					finder = new GoldStandardFinder(
							parsedLine.getHashes().length, MIN_CONSISTENCY,
							MIN_SAMPLES);
				}

				if (!goldStandardFound) {
					// add each hash to the finder one by one
					for (int i = 0; i < parsedLine.getHashes().length; i++) {

						// if the gold standard has been found...
						if (finder.findStandard(i,
								Integer.parseInt(parsedLine.getHashes()[i]),
								parsedLine.getIDs().size())) {

							// get the gold standard
							gold = finder.getGoldStandard();
							goldStandardFound = true;

							// get the cluster
							cluster = getCluster(cluster, gold, parsedLine,
									context);

							// stop adding hashes to the finder
							break;
						}
					}

					// before the gold standard has been found, keep all the
					// combos in a list
					temporaryList.add(parsedLine);

					// after the gold standard is found, separate values
					// according to it
				} else if (parsedLine.atMostNMismatches(MAX_MISMATCHES, gold)) {
					cluster.addIDs(parsedLine.getIDs());
				} else {

					// write the mismatches so they will (hopefully) get grouped
					// with their cluster in another round
					context.write(parsedLine.getKey(), parsedLine.getValue());
				}
			}// end of iteration through the values

			// if gold standard was not found because there were too few values,
			// use what gold you can get - this takes care of corner cases with
			// rare hash values
			if (!goldStandardFound && uniqueIDCounter < MIN_SAMPLES) {
				gold = finder.getGoldStandard();

				// if you can find a combo that is close to the gold, use it for
				// its document to make the cluster
				OldCombo exampleDoc = findExampleDoc(gold, temporaryList);
				if (exampleDoc != null) {
					cluster = getCluster(cluster, gold, exampleDoc, context);
					goldStandardFound = true;
				}

				// if there were enough samples but they were not consistent
				// enough, or somehow the gold didn't match anything well enough
				// to choose an example (doubtful) then emit everything
				// separately
			}

			if (!goldStandardFound || cluster == null) {
				for (OldCombo c : temporaryList) {
					context.write(c.getKey(), c.getValue());
				}

				// a gold standard was found and the cluster is not null
			} else {
				context.write(cluster.getKey(), cluster.getValue());
			}

			if (uniqueIDCounter < 100 && !haveWrittenToFlagFile) {
				// write to the flag file
				fs.createNewFile(new Path(notFinishedFlag));
				haveWrittenToFlagFile = true;
			}

		}

		private OldCombo findExampleDoc(int[] gold, LinkedList<OldCombo> temp) {
			for (OldCombo c : temp) {
				if (c.atMostNMismatches(MAX_MISMATCHES, gold)) {
					temp.remove(c);
					return c;
				}
			}
			return null;

		}

		private OldCombo getCluster(OldCombo givenCluster, int[] gold,
				OldCombo exampleDoc, Context context) throws IOException,
				InterruptedException {
			// prepare a combo to emit for all combos that match
			// the standard well enough
			givenCluster = new OldCombo(gold, exampleDoc, DELIM, DASH);

			// separate the temporary list according to the gold
			// standard
			for (OldCombo combo : temporaryList) {
				if (combo.atMostNMismatches(MAX_MISMATCHES, gold)) {
					givenCluster.addIDs(combo.getIDs());
				} else {

					// write the mismatches so they will
					// (hopefully) get grouped with their
					// cluster in another round
					context.write(combo.getKey(), combo.getValue());
				}
			}
			return givenCluster;
		}
		
		public static void setMaxMisMatches(int newMaxMismatches){
			MAX_MISMATCHES = newMaxMismatches;
		}
		
		public static void setMinSamples(int newMinSamples){
			MIN_SAMPLES = newMinSamples;
		}
		
		public static void setMinConsistency(double newMinConsistency){
			MIN_CONSISTENCY = newMinConsistency;
		}

	}// end Reduce_ClusterDocuments.class

	/*
	 * Job 2
	 */

	/**
	 * 
	 * @author carlchapman
	 * 
	 */
	public static class Map_SeparateOldCombos extends
			Mapper<LongWritable, Text, IntWritable, Text> {
		private String[] tokens;

		/**
		 * 
		 */
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {

			tokens = value.toString().split("\\s+");
			context.write(new IntWritable(Integer.parseInt(tokens[0])),
					new Text(tokens[1]));
		}
	} // end Map_SeparateOldCombos.class

	/**
	 * 
	 * @author carlchapman
	 * 
	 */
	public static class Reduce_WriteAnswers extends
			Reducer<IntWritable, Text, Text, Text> {

		private static int clusterCounter = 0;

		/**
		 * This last reducer assumes that all OldCombos sent to it are similar
		 * documents, so it just clusters them and writes them in the required
		 * format.
		 */
		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			OldCombo cluster = null;
			for (Text t : values) {
				if (cluster == null) {
					cluster = new OldCombo(t.toString(), DELIM, DASH);
				} else {
					OldCombo temp = new OldCombo(t.toString(), DELIM, DASH);
					cluster.addIDs(temp.getIDs());
				}
			}
			int size = cluster.getIDs().size();
			if (size != 100) {
				log("found cluster with size: " + size + " details: "
						+ cluster.getfinalIDs() + "\t" + cluster.getDocument());
			}
			context.write(new Text(cluster.getfinalIDs()),
					new Text(cluster.getDocument()));
			clusterCounter++;
			if (clusterCounter % 100 == 0) {
				log("clusterCounter: " + clusterCounter);
			} else if (clusterCounter > 1000) {
				log("DUPLICATION ERROR!!! clusterCounter: " + clusterCounter);
			}

		}

	}// end Reduce_WriteAnswers.class

	// ************************
	// **** helper methods ****
	// ************************

	/**
	 * logs messages with some decoration so that they stand out in the console
	 * 
	 * @param message
	 *            The message to log.
	 */
	private static void log(String message) {
		log(message, true);
	}

	/**
	 * logs messages
	 * 
	 * @param message
	 *            The message to log.
	 * @param decorate
	 *            True will decorate the log, false will not.
	 */
	private static void log(String message, boolean decorate) {
		if (decorate)
			log.info("**************************");

		log.info(message);

		if (decorate)
			log.info("**************************");
	}

	/**
	 * Takes the standard args array and returns a valid input. If an invalid
	 * input is given, it prints that to the log and exits
	 * 
	 * @param args
	 *            - standard String array of args as passed in through command
	 *            line invocation
	 * @return - a validated input
	 */
	private static String getInput(String[] args) {

		// if there is any input...
		if (args.length > 0) {
			try {
				String input = (args[0]);

				// make sure it is a valid input
				for (String s : validInputs) {
					if (input.equals(s)) {
						return input;
					}
				}
				log("invalid input: " + input);
				System.exit(-1);

				// log exceptions...
			} catch (Exception e) {
				for (StackTraceElement trace : e.getStackTrace()) {
					log(trace.toString(), false);
				}
				System.exit(-1);
			}
		}
		return validInputs[0];
	}

	/**
	 * given hash function
	 * 
	 * @param content
	 *            - the String to be hashed
	 * @param seed
	 *            - a seed to make a unique or repeatable hash function
	 * @return - an int hash of the String
	 */
	private static int hash(String content, int seed) {

		int m = 0x5bd1e995;
		int r = 24;

		int len = content.length();
		byte[] work_array = null;

		int h = seed ^ content.length();

		int offset = 0;

		while (len >= 4) {
			work_array = new byte[4];
			ByteBuffer buf = ByteBuffer.wrap(content.substring(offset,
					offset + 4).getBytes());

			int k = buf.getInt();
			k = k * m;
			k ^= k >> r;
			k *= m;
			h *= m;
			h ^= k;

			offset += 4;
			len -= 4;
		}

		switch (len) {
		case 3:
			h ^= work_array[2] << 16;
		case 2:
			h ^= work_array[1] << 8;
		case 1:
			h ^= work_array[0];
			h *= m;
		}

		h ^= h >> 13;
		h *= m;
		h ^= h >> 15;

		return h;
	}

}